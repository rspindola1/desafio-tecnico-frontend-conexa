import axios from "axios";
import React, { createContext, useState, useEffect, useContext } from "react";

interface AuthContextData {
  signed: boolean;
  user: string | null;
  token: string | null;
  Login(user: object): Promise<void>;
  Logout(): void;
}

interface IProps {
  children: React.ReactNode;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider: React.FC<IProps> = ({ children }) => {
  const [user, setUser] = useState<string | null>(null);
  const [token, setToken] = useState<string | null>(null);

  useEffect(() => {
    const storagedUser = localStorage.getItem("@App:user");
    const storagedToken = localStorage.getItem("@App:token");
    
    if (storagedToken && storagedUser) {
      setUser(JSON.parse(storagedUser));
      setToken(storagedToken);
    }
  }, []);

  async function Login(userData: object) {
    const response = await axios.post("http://localhost:3333/login", userData);
    
    setUser(response.data.user);
    localStorage.setItem("@App:user", JSON.stringify(response.data.name));
    localStorage.setItem("@App:token", response.data.token);
  }

  function Logout() {
    setUser(null);
    localStorage.removeItem('@App:user');
    localStorage.removeItem('@App:token');
  }

  return (
    <AuthContext.Provider
      value={{ signed: Boolean(user), user, token, Login, Logout }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);

  return context;
}
