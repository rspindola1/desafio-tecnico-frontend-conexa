import Logo from "../../assets/images/logo-conexa.svg";
import styles from "./styles.module.scss";
import Button from "../Button";
import { useAuth } from "../../contexts/auth";

const Header: React.FC = () => {
  const { signed, user, Logout } = useAuth();
  const logout = async () => {
    await Logout();
    window.location.href ='/login'
  };

  return (
    <header className={styles.header}>
      <img src={Logo} alt="Logo da Conexa" />
      {signed && (
        <div>
          <span>{user ? user : ''}</span>
          <Button onClick={() => logout()} className="btn outline ml-1">
            Sair
          </Button>
        </div>
      )}
    </header>
  );
};

export default Header;
