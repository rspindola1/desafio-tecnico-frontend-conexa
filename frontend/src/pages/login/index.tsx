import { useEffect } from "react";
import { useNavigate } from "react-router";
import { useAuth } from "../../contexts/auth";
import { LoginForm } from "./LoginForm";
import styles from "./styles.module.scss";

import infoImg from "./svg/info.svg";

const Login: React.FC<{}> = () => {
  const { signed } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    if (signed) {
      navigate("/", {replace: true});
    }
  }, [signed]);

  return (
    <div className={styles.content}>
      <div className={styles.info}>
        <h1 className={styles.title}>Faça Login</h1>
        <img src={infoImg} className={styles.svg} alt="Imagem" />
      </div>
      <LoginForm />
    </div>
  );
};

export default Login;
