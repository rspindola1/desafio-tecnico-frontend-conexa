import * as Yup from "yup";
import { useFormik } from "formik";
import Button from "../../components/Button";
import styles from "./styles.module.scss";
import { useState } from "react";
import { useAuth } from "../../contexts/auth";

export const LoginForm: React.FC<{}> = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const { Login } = useAuth();

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().email().required(),
      password: Yup.string().required(),
    }),
    onSubmit: async (values) => {
      setMessage("");
      setLoading(true);

      await Login(values);
      window.location.href = "/";
    },
  });

  return (
    <div className={styles.formContent}>
      <form onSubmit={formik.handleSubmit}>
        <div className={styles.inputCol}>
          <label htmlFor="email">Email</label>
          <input
            type="text"
            name="email"
            id="email"
            placeholder="Digite seu e-mail"
            onChange={formik.handleChange}
            value={formik.values.email}
          />
          {formik.touched.email && formik.errors.email ? (
            <div>{formik.errors.email}</div>
          ) : null}
        </div>
        <div className={styles.inputCol}>
          <label htmlFor="password">Senha</label>
          <input
            type="password"
            name="password"
            id="password"
            onChange={formik.handleChange}
            value={formik.values.password}
          />
          {formik.touched.password && formik.errors.password ? (
            <div>{formik.errors.password}</div>
          ) : null}
        </div>
        <div className={styles.buttonBox}>
          <Button className="btn default">
            {loading ? <span>Carregando</span> : <span>Entrar</span>}
          </Button>
        </div>
      </form>
    </div>
  );
};
