import axios from "axios";
import { useEffect, useState } from "react";
import Button from "../../components/Button";
import { format } from "date-fns";
import "./style.scss";
import { useNavigate } from "react-router";
import { useAuth } from "../../contexts/auth";

export interface IAppointment {
  id: number;
  patientId: string;
  date: Date;
  patient: IPatient;
}

export interface IPatient {
  id: string;
  first_name: string;
  last_name: string;
}

export const DoctorHome: React.FC = () => {
  const { signed, token } = useAuth();
  const navigate = useNavigate();

  const [patients, setPatients] = useState<IAppointment[] | undefined>(
    undefined
  );

  useEffect(() => {
    if (!signed) {
      navigate("/login", { replace: true });
    }

    console.log(token);

    if (token) {
      axios
        .get("http://localhost:3333/consultations?_expand=patient", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          const { data } = res;
          setPatients(data);
        });
    }
  }, []);

  return (
    <div className="container">
      <h1 className="title">Consultas</h1>
      <div className="main">
        {patients?.length === 0 ? (
          <div className="no-data">
            <span className="span">Não há nenhuma consulta agendada</span>
          </div>
        ) : (
          <div className="info">
            <h2>{patients?.length} consultas agendadas</h2>
            {patients?.map((p: IAppointment, index) => (
              <div className="item" key={index}>
                <div className="details">
                  <p className="name">
                    {p.patient.first_name} {p.patient.last_name}
                  </p>
                  <p className="date">
                    {format(new Date(p.date), "dd/MM/yyyy' às 'HH:mm")}
                  </p>
                </div>
                <Button className={"btn default"}>Atender</Button>
              </div>
            ))}
          </div>
        )}
      </div>
      <div className="footer">
        <Button className="btn outline ml-1">Ajuda</Button>
        <Button className="btn default mr-1">Agendar Consulta</Button>
      </div>
    </div>
  );
};
