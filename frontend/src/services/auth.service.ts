import axios from "axios";
const API_URL = "http://localhost:3333/";

export const login = (data: { email: string; password: string }) => {
  return axios.post(API_URL + "login", data).then((response) => {
    console.log(response.data);

    if (response.data.token) {
      localStorage.setItem("user", JSON.stringify(response.data));
    }
    return response.data;
  });
};

export const logout = () => {
  localStorage.removeItem("user");
};

export const getCurrentUser = () => {
  const userStr = localStorage.getItem("user");
  if (userStr) return JSON.parse(userStr);
  return null;
};
